function dropdown() {
  var dropdownMenu = document.getElementById("dropdown-nav-menu");
  var dropdownButton = document.getElementById("burger-button");
  if (dropdownMenu.style.display === "block") {
    dropdownMenu.style.display = "none";
    dropdownButton.src = "./assets/burgermenu.svg";
  } else {
    dropdownButton.src = "./assets/close.svg";
    dropdownMenu.style.display = "block";
  }
}



if (window.location.pathname.endsWith("/map.html")) {
  const tooltipOptions = {
    track: true,
    content: function () {
      var element = $(this);
      const title = element.attr("title");
      const subtitle = element.attr("subtitle");
      const cheeseLogo = element.attr("cheese-logo");
      const container = document.createElement("div");
      const header = document.createElement("h1");
      const subHeader = document.createElement("h2");
      const img = document.createElement("img");


      container.style.textAlign = "center";
      container.style.margin = "0.5em";
      
      img.src = cheeseLogo;
      img.style.width = "100px";

      container.append(header);
      container.append(subHeader);
      container.append(img);

      header.textContent = title;
      subHeader.textContent = subtitle;

      return container;
    },
  };

  $(function () {
    $(document).tooltip(tooltipOptions);

    $('.UK, .DE, .NW, .PO, .SP, .IT, .SW, .FR, .GR, .NE').on('click', function() {
        $('.UK, .DE, .NW, .PO, .SP, .IT, .SW, .FR, .GR, .NE').css('fill', 'var(--secondary-color)');
        var element = $(this);
        element.css('fill', 'var(--feature-bg-color)');

        const title = element.attr("title");
        const subtitle = element.attr("subtitle");
        const cheeseLogo = element.attr("cheese-logo");
    
        $('#mobile-tooltip-container-title').text(title);
        $('#mobile-tooltip-container-subtitle').text(subtitle);
        $('#mobile-tooltip-container-cheeselogo').attr('src', cheeseLogo);
    })
  });
}

// console.log(querySelectorAll)
